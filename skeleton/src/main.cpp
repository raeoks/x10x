//
//  main.cpp
//  Skeleton
//
//  Created by Ranjeet Singh
//  Copyright (c) 2015 Raeoks Engineering. All rights reserved.
//

#include <iostream>
#include <sstream>

#include "main.hpp"

int main(int argc, const char * argv[]) {
  if (argc != 2) {
    std::cerr << "Usage: skeleton <number>"<< std::endl;
    return -1;
  }
  std::istringstream istream(argv[1]);
  int number;
  if(!(istream >> number)) {
    std::cerr << "Invalid number "<< argv[1] << std::endl;
    std::cerr << "Usage: skeleton <number>"<< std::endl;
    return -2;
  }
  std::cout << skeleton::Factorial(number) << std::endl;
  return 0;
}
