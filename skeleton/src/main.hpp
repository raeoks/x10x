//
//  main.hpp
//  Skeleton
//
//  Created by Ranjeet Singh.
//  Copyright (c) 2015 Raeoks Engineering. All rights reserved.
//

#ifndef SKELETON_MAIN_HPP_
#define SKELETON_MAIN_HPP_

namespace skeleton {
  int Factorial(int number) {
    if (number <= 1) {
      return 1;
    }
    return number * Factorial(number-1);
  }
}

#endif // SKELETON_MAIN_HPP_
