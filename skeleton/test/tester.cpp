//
//  tester.cpp
//  Skeleton Tester
//
//  Created by Ranjeet Singh.
//  Copyright (c) 2015 Raeoks Engineering. All rights reserved.
//

#define CATCH_CONFIG_MAIN

#include "catch.hpp"

#include "../src/main.hpp"

using namespace skeleton;

TEST_CASE("Testing Skeleton") {

  SECTION("test Factorial") {

    SECTION("number is 0") {
      REQUIRE(Factorial(0) == 1);
    }

    SECTION("number is 1") {
      REQUIRE(Factorial(1) == 1);
    }

    SECTION("number is greater than 1") {
      REQUIRE(Factorial(5) == 120);
    }
  }
}
